<?php

Route::get('/',        'frontController@index');
Route::get('home',     'frontController@index');
Route::get('test',     'frontController@test');
Route::get('login',    'frontController@login');
Route::get('registro', 'frontController@registro');
Route::get('app', 'frontController@homeApp');
Route::get('log', 'LogController@store');
Route::get('logout', 'LogController@logout');


Route::resource('vehiculo', 'carController');
Route::resource('usuario', 'userController');
