@extends('layouts.base')

@section('title', $title)
@section('desc', $desc)

@section('content')
<div class="page-wrap has-header big-bg aligner">
    
  <!-- LOGIN FORM -->
    <div class="container">
      <div class="form-card v-center"><img src="images/logo.png">
        <h1>Iniciar Sesion</h1>

        {!!Form::open(['route'=>'login', 'method'=>'POST'])!!}
          <div class="form-group">
            <input type="text" placeholder="Usuario" required autofocus class="form-control">
          </div>
          <div class="form-group">
            <input type="password" placeholder="Contraseña" required class="form-control">
          </div>
          {!!Form::submit('Iniciar sesion',['class'=>'action'])!!}
        {!!Form::close()!!}

        <div class="text-center"><a href="/registro">Registrate </a></div>
      </div>

    </div>
  <!-- LOGIN FORM -->
</div>
@stop
    