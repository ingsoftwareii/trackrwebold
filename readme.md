# Trackr
----------
Proyecto de ing del software II

## Repositorios
- Produccion: `ssh://57059b737628e1f6840001e5@trackr-soft2.rhcloud.com/~/git/trackr.git/`
- Desarrollo: `git@bitbucket.org:ingsoftwareii/trackrweb.git`

## Desarrollo
Desde consola, teniendo instalado nodejs con npm y composer ejecutas:

- Dependencias Globales:
```
$ composer global require "laravel/installer"
$ npm install -g bower gulp-cli stylus
```

- Instalar dependencias del proyecto:
```
$ composer install
$ npm install 
$ bower install
```

## Gulp Tasks

```
$ gulp
$ php artisan serve
```