


// TEST MAP
var ruta = [];
var map;
var distanciaTotal = 0;

var testMap = function (){

    $.getJSON("/js/posiciones.json", function(data) {
      $.each( data, function( key, val ) {
        ruta.push( val );
      });
    });

    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "positionClass": "toast-bottom-right"
    };

    var gMapApi = setInterval( function(){
        if (typeof google === 'object' && typeof google.maps === 'object'){
            clearInterval(gMapApi);
            google.maps.event.addDomListener(window, "load", initTestMap);
        }
    }, 250); 
}

// INICIAR MAPA
var initTestMap = function () {
    var mapDiv = document.getElementById("testMap");
    if (mapDiv === null)
        return;
    var uneg = {
        lat: 8.2719748,
        lng: -62.7356503
    };
    var mapOpts = {
        center: uneg,
        zoom: 14,
        // scrollwheel: false,
        scaleControl: true
    };

    map = new google.maps.Map(mapDiv, mapOpts);

    // MAPA CARGADO
    google.maps.event.addListenerOnce(map, 'idle', function(){
        var count = 0;
        var limit = ruta.length -1;

        setTimeout( function(){
            var interval = setInterval( function(){
                count++;

                addMarker( ruta[count] );
                if ( count >= 1)
                    traceRoute(ruta[count-1], ruta[count]);

                if ( count == limit)
                    clearInterval(interval);
            }, 2500);     
        }, 1000);
        // CADA 2.5 SEGUNDOS
    });
};
// INICIAR MAPA


// AGREGAR MARKER
var addMarker = function(ubicacion){
    toastr.info( ubicacion.lat + "  ,  " + ubicacion.lng );
    console.log( ubicacion );
    var marker = new google.maps.Marker({
        map:           map,
        position:      ubicacion,
        animation:     google.maps.Animation.DROP,
        markerOptions: {}
    });
    marker.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
            infowindow.setContent("Texto prueba con <b>HTML</b>");
            infowindow.open(map, marker);
        };
    })(marker));
};
// AGREGAR MARKER


// TRAZAR RUTA
var traceRoute = function(origin, destino){
    var direccionDisplay = new google.maps.DirectionsRenderer({
        map: map,
        preserveViewport: true,
        suppressMarkers: true,
        polylineOptions:{
            strokeColor: '#18BC9C',
            strokeOpacity: 0.75,
            strokeWeight: 4
        }
    });

    var request = {
        origin:      origin,
        destination: destino,
        travelMode:  google.maps.TravelMode.DRIVING
    };

    var direccionService = new google.maps.DirectionsService();
    direccionService.route( request, function (response, status) {
        if( status == google.maps.DirectionsStatus.OK ){
            direccionDisplay.setDirections( response );

            updateDistance( response.routes[0].legs[0].distance.value );
        }
    });
};
// TRAZAR RUTA


// UPDATE DISTANCE
var distanceTxt = $('.testMap-container .distance span');
var updateDistance = function( value ){
    distanciaTotal += value;
    distanceTxt.text(distanciaTotal / 1000);
};
// UPDATE DISTANCE