
// carFormMap
var circles = [];
var carFormMap = function (){

    var gMapApi = setInterval( function(){
        if (typeof google === 'object' && typeof google.maps === 'object'){
            google.maps.event.addDomListener(window, "load", initCarFormMap);
            clearInterval(gMapApi);
        }
    }, 250); 
};

// INICIAR MAPA
var initCarFormMap = function () {
    var mapDiv = document.getElementById("input_map");
    if (mapDiv === null)
        return;
    var uneg = {
        lat: 8.2719748,
        lng: -62.7356503
    };
    var mapOpts = {
        center: uneg,
        zoom: 14,
        disableDefaultUI:true
    };

    map = new google.maps.Map(mapDiv, mapOpts);

    // MAPA CARGADO
    google.maps.event.addListenerOnce(map, 'idle', function(){

        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.MARKER,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                  google.maps.drawing.OverlayType.CIRCLE,
                ]
            },
            circleOptions: {
                strokeColor:   '#0048b3',
                strokeOpacity: 0.8,
                strokeWeight:  2,
                fillColor:     '#3385ff',
                fillOpacity:   0.35,
                clickable:     true,
                editable:      true,
                zIndex: 1
            }
        });
        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'circlecomplete', function(e) {
            circles.push(e);

            google.maps.event.addListener(circles[circles.length -1], 'center_changed', function() {
                console.log("center_changed", circles[0].getCenter().lat(), circles[0].getCenter().lng());
            });

            google.maps.event.addListener(circles[circles.length -1], 'radius_changed', function() {
                console.log("radius_changed", circles[0].getRadius());
            });

            google.maps.event.addListener(circles[circles.length -1], 'click', function() {
                var i = circles.indexOf(this);
                if(i != -1) {
                    circles.splice(i, 1);
                }
                e.setMap(null);
                console.log("Removed circle", circles.length);
            });
        });

    });
};
// INICIAR MAPA
