@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)

@section('content')
<div class="page-wrap has-header">
    
  <!-- LOGIN FORM --> 
  <div class="container form-registro">
    <h1>Editar vehiculo</h1>
    <hr class="full left">

    {!!Form::model($car, [
      'route'  => ['vehiculo.update', $car->id],
      'method' => 'PUT',
      'class'  => 'form-horizontal'
    ])!!}

      @include('car.form')
      <div class="form-group text-center">
        <div class="col-sm-offset-2 col-sm-10">
          {!!Form::submit('Aceptar',['class' => 'btn btn-primary'])!!}
          {!!Form::reset('Cancelar',['class' => 'btn btn-warning'])!!}
        </div>
      </div>
    {!!Form::close()!!}

    {!!Form::open([
      'route'  => ['vehiculo.destroy', $car->id],
      'method' => 'DELETE',
      'class'  => 'form-horizontal'
    ])!!}
      <div class="form-group text-center">
        <div class="col-sm-offset-2 col-sm-10">
          {!!Form::submit('Eliminar',['class' => 'btn btn-danger'])!!}
        </div>
      </div>
    {!!Form::close()!!}
  </div>
  <!-- LOGIN FORM -->

</div>
@stop
   
