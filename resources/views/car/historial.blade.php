@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)

@section('content')
	<div class="page-wrap has-header">
    
    <div class="container">
      <h1 class="text-center">Historial de ubicaciones</h1>
      <p class="text-center"> {{$car->marca}} // {{$car->modelo}} // {{$car->placa}} </p>
      <hr>

      <div class="panel panel-default">
        <div class="panel-heading">Fecha</div>
        <div class="panel-body">
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>Direccion</th>
                <th>Hora</th>
                <th>Velocidad</th>
                <th>Distancia</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
@stop
    
