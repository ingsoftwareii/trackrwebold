@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)


@section('content')
<div class="page-wrap has-header">
  <div class="container">
    <h1 class="text-center">Vehiculos</h1>
    <hr>  

    <!-- MENSAJE -->
    @if( Session::has('message'))
      <div class="alert alert-success alert-dismissible text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        {{ Session::get('message')}}
      </div>
    @endif
    <!-- MENSAJE -->

    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>IMEI</th>
          <th>Marca</th>
          <th>Modelo</th>
          <th>Placa</th>
          <th><i class="glyphicon glyphicon-option-horizontal"></i></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>
              {!! link_to_route('vehiculo.create', 'Agregar nuevo vehiculo',null, ['class' => 'btn btn-default btn-sm btn-primary'])!!}
          </td>
        </tr>
      @foreach($vehiculos as $car)
        <tr>
          <td>{{$car->imei}}</td>
          <td>{{$car->marca}}</td>
          <td>{{$car->modelo}}</td>
          <td>{{$car->placa}}</td>
          <td class="shrink">
            {!! link_to_route('vehiculo.ubicacion', 'Ver Ubicacion', $car->imei, ['class' => 'btn btn-default btn-sm btn-primary'])!!}
            {!! link_to_route('vehiculo.historial', 'Ver Historial', $car->id, ['class' => 'btn btn-default btn-sm btn-success'])!!}
            {!! link_to_route('vehiculo.edit', 'Editar Vehiculo', $car->id, ['class' => 'btn btn-default btn-sm btn-warning'])!!}
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>    

</div>
@stop
   
