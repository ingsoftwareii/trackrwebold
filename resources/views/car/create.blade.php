@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)

@section('content')
<div class="page-wrap has-header">
    
  <!-- LOGIN FORM --> 
  <div class="container form-registro">
    <h1>Agregar vehiculo</h1>
    <hr class="full left">

    {!!Form::open([
      'route'  => 'vehiculo.store',
      'method' => 'POST',
      'class'  => 'form-horizontal'
    ])!!}

    @include('car.form')
    <div class="form-group text-center">
      <div class="col-sm-offset-2 col-sm-10">
        {!!Form::submit('Aceptar',['class' => 'btn btn-primary'])!!}
        {!!Form::reset('Cancelar',['class' => 'btn btn-warning'])!!}
      </div>
    </div>
    
    {!!Form::close()!!}
  </div>
  <!-- LOGIN FORM -->

</div>
@stop
   
