@extends('layouts.base-loged')

@section('title', $title)
@section('desc', $desc)

@section('content')
	<div class="page-wrap has-header">
    
    <div class="container">
      <h1 class="text-center">Estadisticas</h1>
      <hr>

      @foreach($cars as $car)
      <div class="panel panel-default">
        <div class="panel-heading">{{$car->marca}} // {{$car->modelo}} // {{$car->placa}}</div>
        <div class="panel-body">
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>Velocidad promedio:</th>
                <th>Distancia recorrida:</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td> {{$car->marca} km/h</td>
                <td>km</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      @endforeach

    </div>
  </div>
@stop
    
