

<div class="form-group">
  {!!Form::label('imei', 'IMEI', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('imei',null ,['class' => 'form-control', 'placeholder' => "Introduzca IMEI del dispositivo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('telefono', 'Telefono', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('telefono',null ,['class' => 'form-control', 'placeholder' => "Introduzca linea telefonica del dispositivo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('marca', 'Marca', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('marca',null ,['class' => 'form-control', 'placeholder' => "Introduzca la marca del vehiculo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('modelo', 'Modelo', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('modelo',null ,['class' => 'form-control', 'placeholder' => "Introduzca el modelo del vehiculo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('placa', 'Placa', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('placa',null ,['class' => 'form-control', 'placeholder' => "Introduzca la placa del vehiculo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('color', 'Color', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('color',null ,['class' => 'form-control', 'placeholder' => "Introduzca el color del vehiculo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('lim_velocidad', 'Limite de velocidad', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('lim_velocidad',null ,['class' => 'form-control', 'placeholder' => "Introduzca el limite de velocidad", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  <label for="input_map" class="control-label col-sm-2">Restriccion de localidad</label>
  <div class="col-sm-10">
    <div id="input_map" name="input_map" class="form-control form-map"></div>
  </div>
</div>

<div class="form-group">
  {!!Form::label('lim_velocidad', 'Limite de velocidad', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('lim_velocidad',null ,['class' => 'form-control', 'placeholder' => "Introduzca el limite de velocidad", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>