

<div class="form-group">
  {!!Form::label('imei', 'IMEI', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('imei',null ,['class' => 'form-control', 'placeholder' => "Introduzca IMEI del dispositivo", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('longitud', 'Longitud', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('longitud',null ,['class' => 'form-control', 'placeholder' => "Longitud", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('latitud', 'Latitud', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('latitud',null ,['class' => 'form-control', 'placeholder' => "Latitud", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('fecha', 'Fecha', ["class" => "control-label col-sm-2"])!!}
  <div class="col-sm-10">
    {!!Form::text('fecha',null ,['class' => 'form-control', 'placeholder' => "Fecha", 'required' => '', 'autofocus' => ''])!!}
  </div>
</div>
