@extends('layouts.base')

@section('title', $title)
@section('desc', $desc)

@section('content')


<div class="page-wrap has-header">
	<!-- HERO TITLE -->
	<section class="jumbotron hero-title text-center">
		<div class="container">
			<div class="page-header">
				<img src="images/map-logo.png" width="100px" alt="">
				<h1>TRACKR </h1>
				<p>Sistema de tracking y seguridad.</p>
			</div>
		</div>
		<div id="homeMap"></div>
	</section>
	<!-- HERO TITLE -->

	<!--  -->
	<section class="container">
		<div class="row">

			<div class="col-md-4">
				<div class="card">
					<img src="{{ URL::asset('images/logo.png') }}">
					<h2 class="title">Localiza</h2>
					<p class="content">Sientete mas seguro localizando tu vehiculo, donde quiere que este, en cuestiones de segundos.</p>
				</div>
			</div>

			<div class="col-md-4">
				<div class="card">
					<img src="{{ URL::asset('images/map-logo.png') }}">
					<h2 class="title">Visualiza</h2>
					<p class="content">Visualiza la ubicaciones de tus vehiculos en un mapa con detalles de direcciones y rutas recorridas.</p>
				</div>
			</div>

			<div class="col-md-4">
				<div class="card">
					<img src="{{ URL::asset('images/report.png') }}">
					<h2 class="title">Mantente Informado</h2>
					<p class="content">Consulta todas los reportes que tenemos para ti, velocidad promedio, distancia recorrida, incluyendo el modo <b>flota</b> </p>
				</div>
			</div>
			<div class="clearfix"></div>

			<p class="text-center">
				<a href="/login" class="btn btn-success btn-lg">Inicia sesion</a>
			</p>
		</div>

	</section>


	<section class="jumbotron text-center mobile-app">
		<div class="container">
			<div class="page-header col-md-6">
				<a href="#">
					<h1>Trackr App </h1>
					<p>Descarga nuestra app movil.</p>
				</a>
			</div>
			<div class=" col-md-6">
				<a href="#"><img src="{{ URL::asset('images/mobile-app.png') }}"></a>
			</div>
		</div>
	</section>


</div>






@stop
