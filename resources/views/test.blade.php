@extends('layouts.base')

@section('title', $title)
@section('desc', $desc)

@section('content')
	<div class="has-header"></div>
	<div class="testMap-container">
        <div id="testMap"></div>
        <div class="distance">
         	Distancia total recorrida:
         	<span>0</span>Km
         </div>
    </div>
@stop
    
