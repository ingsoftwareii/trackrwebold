<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			$table->string('imei_car');
			$table->foreign('imei_car')->references('imei')->on('cars');
			$table->double('longitud',10,6);
			$table->double('latitud',10,6);
			$table->string('direccion');
			$table->string('fecha_hora');
			$table->integer('velocidad');
			$table->integer('distancia');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locations');
	}

}

