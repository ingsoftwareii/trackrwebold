<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model {

  protected $table = 'cars';
  protected $fillable = [
    'imei', 
    'telefono',
    'marca',
    'modelo',
    'placa',
    'color',
    'estado',
    'lim_velocidad',
    'user_id',
  ];

}
