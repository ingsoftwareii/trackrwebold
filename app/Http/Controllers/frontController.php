<?php namespace App\Http\Controllers;

class frontController extends Controller {
	public $title = "Trackr";
	public $descr = "Sistema de trackeo";

	public function __construct(){
        $this->middleware('auth',['only' => 'homeApp']);    
	    $this->middleware('guest',['only' => ['index','test','login','registro']]);    
	 }


	// HOME
	public function index()
	{
	    return view('home', [
	      'title' => $this->title." | Inicio",
	      'desc'  => $this->descr,
	      'slug'  => "inicio"
	    ]);
	}
	// HOME
	

	// TEST
	public function test()
	{
	    return view('test', [
	      'title' => $this->title." | Map test",
	      'desc'  => $this->descr,
	      'slug'  => "test"
	    ]);
	}	
	// TEST
	
	
	// LOGIN
	public function login()
	{
	    return view('login', [
	      'title' => $this->title." | Inicio sesion",
	      'desc'  => $this->descr,
	      'slug'  => "login"
	    ]);
	}	
	// LOGIN


	// REGISTRO
	public function registro()
	{
	    return view('registro', [
	      'title' => $this->title." | Registro",
	      'desc'  => $this->descr,
	      'slug'  => "registro"
	    ]);
	}
	// REGISTRO

	public function homeApp()
	{
	    return view('layouts.base-loged', [
	      'title' => $this->title." | Registro",
	      'desc'  => $this->descr,
	      'slug'  => "registro"
	    ]);
	}

}



  
