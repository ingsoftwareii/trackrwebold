<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Car;
use App\User;
use App\Location;

use Illuminate\Http\Request;
use Session;
use Redirect;
use Auth;
use DB;


class carController extends Controller {
	public $title = "Trackr";
	public $descr = "Sistema de trackeo";

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//$vehiculos = Car::All();
		$user 	   = Auth::user()->id;
		$vehiculos = DB::table('cars')->where('user_id',$user)->get();
		return view('car.index', [
	    'title' => $this->title." | Vehiculos",
	    'desc'  => $this->descr,
	    'slug'  => "car.index",
	  ], compact('vehiculos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('car.create', [
			'title' => $this->title." | Agregar vehiculo",
			'desc'  => $this->descr,
			'slug'  => "car.create"
			]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		Car::create([
			'imei'          => $request['imei'], 
	    	'telefono'      => $request['telefono'],
	    	'marca'         => $request['marca'],
	    	'modelo'        => $request['modelo'],
	    	'placa'         => $request['placa'],
	    	'color'         => $request['color'],
	    	'estado'        => 'Encendido',
	    	'lim_velocidad' => $request['lim_velocidad'],
	    	'user_id' 		=> Auth::user()->id
		]);
		return redirect('/vehiculo')->with('message', 'store');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$vehiculo = Car::find($id);
		return view('car.edit', [
			'title' => $this->title." | Agregar vehiculo",
			'desc'  => $this->descr,
			'slug'  => "car.create",
			'car'   => $vehiculo
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$vehiculo = Car::find($id);
		$vehiculo->fill($request->all());
		$vehiculo->save();
		
		Session::flash('message', 'Vehiculo actualizado con exito');
		return Redirect::to('/vehiculo');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Car::destroy($id);
		Session::flash('message', 'Vehiculo eliminado con exito');
		return Redirect::to('/vehiculo');
	}


	public function ubicacion($imei)
	{
		$vehiculo = DB::table('locations')->where('imei_car',$imei)->get();
		//return $vehiculo;
		return view('car.ubicaciones', [
			'title' => $this->title." | Ubicacion de vehiculo",
			'desc'  => $this->descr,
			'slug'  => "car.ubicaciones",
			'car'   => $vehiculo
			],compact('vehiculo','imei'));
	}

	public function historial($id)
	{	
		$vehiculo = Car::find($id);
		return view('car.historial', [
			'title' => $this->title." | Historial de ubicaciones de vehiculo",
			'desc'  => $this->descr,
			'slug'  => "car.historial",
			'car'   => $vehiculo
			]);
	}

	public function estadisticas()
	{	
		$user     = Auth::user()->id;
		$cars = DB::table('cars')->where('user_id',$user)->get();
			return view('estadisticas', [
		    'title' => $this->title." | Estadisticas",
		    'desc'  => $this->descr,
		    'slug'  => "estadisticas",
		  ], compact('cars'));		
		
		/*foreach ($vehiculos as $vehi ) {
			$cars 	  = DB::table('locations')->where('imei_car',$vehi->imei)->get();
			return view('estadisticas', [
		    'title' => $this->title." | Estadisticas",
		    'desc'  => $this->descr,
		    'slug'  => "estadisticas",
		  ], compact('cars','vehi'));
		}*/
	}

}
