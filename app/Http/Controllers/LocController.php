<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Car;
use App\User;
use App\Location;

use Illuminate\Http\Request;

use Session;
use Redirect;
use Auth;
use DB;


class LocController extends Controller {
	public $title = "Trackr";
	public $descr = "Sistema de trackeo";
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('location.create', [
	      'title' => $this->title." | Registro",
	      'desc'  => $this->descr,
	      'slug'  => "registro"
	    ]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		Location::create([
			'imei_car'      => $request['imei'], 
	    	'longitud'      => $request['longitud'],
	    	'latitud'       => $request['latitud'],
	    	'direccion'     => 'un BTR a 180 por la guayana',
	    	'fecha_hora'    => $request['fecha'],
	    	'velocidad'     => 0,
	    	'distancia'     => 0
		]);
		return redirect('/ubica/create');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
