<?php

Route::get('/',        'frontController@index');
Route::get('home',     'frontController@index');
Route::get('test',     'frontController@test');
Route::get('login',    'frontController@login');
Route::get('app',    'frontController@homeApp');
Route::get('registro', 'frontController@registro');
Route::get('logout', 'LogController@logout');

Route::get('vehiculo/{imei}/ubicacion', ['as' => 'vehiculo.ubicacion', 'uses' => 'carController@ubicacion']);
//Route::get('ubicaciones', 'carController@ubicacion');
Route::get('vehiculo/{id}/historial', ['as' => 'vehiculo.historial', 'uses' => 'carController@historial']);

Route::get('ubicacion/{id}/show', ['as' => 'ubicacion.show', 'uses' => 'LocController@show']);

Route::get('estadisticas', ['as' => 'estadisticas', 'uses' => 'carController@estadisticas']);

Route::resource('vehiculo', 'carController');
Route::resource('usuario', 'userController');
Route::resource('log', 'LogController');
Route::resource('ubica', 'LocController');

