<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {

	protected $table = 'locations';
	protected $fillable = [
	    'imei_car', 
	    'longitud',
	    'latitud',
	    'direccion',
	    'fecha_hora',
	    'velocidad',
	    'distancia'
	];

}
