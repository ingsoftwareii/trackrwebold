
$( document ).ready(function(){

	testMap();
	$('.testMap-container').height( $(window).height() - 60 - $('footer').outerHeight() );

  carFormMap();
});


// carFormMap
var circles = [];
var carFormMap = function (){

    var gMapApi = setInterval( function(){
        if (typeof google === 'object' && typeof google.maps === 'object'){
            google.maps.event.addDomListener(window, "load", initCarFormMap);
            clearInterval(gMapApi);
        }
    }, 250); 
};

// INICIAR MAPA
var initCarFormMap = function () {
    var mapDiv = document.getElementById("input_map");
    if (mapDiv === null)
        return;
    var uneg = {
        lat: 8.2719748,
        lng: -62.7356503
    };
    var mapOpts = {
        center: uneg,
        zoom: 14,
        disableDefaultUI:true
    };

    map = new google.maps.Map(mapDiv, mapOpts);

    // MAPA CARGADO
    google.maps.event.addListenerOnce(map, 'idle', function(){

        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.MARKER,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                  google.maps.drawing.OverlayType.CIRCLE,
                ]
            },
            circleOptions: {
                strokeColor:   '#0048b3',
                strokeOpacity: 0.8,
                strokeWeight:  2,
                fillColor:     '#3385ff',
                fillOpacity:   0.35,
                clickable:     true,
                editable:      true,
                zIndex: 1
            }
        });
        drawingManager.setMap(map);

        google.maps.event.addListener(drawingManager, 'circlecomplete', function(e) {
            circles.push(e);

            google.maps.event.addListener(circles[circles.length -1], 'center_changed', function() {
                console.log("center_changed", circles[0].getCenter().lat(), circles[0].getCenter().lng());
            });

            google.maps.event.addListener(circles[circles.length -1], 'radius_changed', function() {
                console.log("radius_changed", circles[0].getRadius());
            });

            google.maps.event.addListener(circles[circles.length -1], 'click', function() {
                var i = circles.indexOf(this);
                if(i != -1) {
                    circles.splice(i, 1);
                }
                e.setMap(null);
                console.log("Removed circle", circles.length);
            });
        });

    });
};
// INICIAR MAPA




// TEST MAP
var ruta = [];
var map;
var distanciaTotal = 0;

var testMap = function (){

    $.getJSON("/js/posiciones.json", function(data) {
      $.each( data, function( key, val ) {
        ruta.push( val );
      });
    });

    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "positionClass": "toast-bottom-right"
    };

    var gMapApi = setInterval( function(){
        if (typeof google === 'object' && typeof google.maps === 'object'){
            clearInterval(gMapApi);
            google.maps.event.addDomListener(window, "load", initTestMap);
        }
    }, 250); 
}

// INICIAR MAPA
var initTestMap = function () {
    var mapDiv = document.getElementById("testMap");
    if (mapDiv === null)
        return;
    var uneg = {
        lat: 8.2719748,
        lng: -62.7356503
    };
    var mapOpts = {
        center: uneg,
        zoom: 14,
        // scrollwheel: false,
        scaleControl: true
    };

    map = new google.maps.Map(mapDiv, mapOpts);

    // MAPA CARGADO
    google.maps.event.addListenerOnce(map, 'idle', function(){
        var count = 0;
        var limit = ruta.length -1;

        setTimeout( function(){
            var interval = setInterval( function(){
                count++;

                addMarker( ruta[count] );
                if ( count >= 1)
                    traceRoute(ruta[count-1], ruta[count]);

                if ( count == limit)
                    clearInterval(interval);
            }, 2500);     
        }, 1000);
        // CADA 2.5 SEGUNDOS
    });
};
// INICIAR MAPA


// AGREGAR MARKER
var addMarker = function(ubicacion){
    toastr.info( ubicacion.lat + "  ,  " + ubicacion.lng );
    console.log( ubicacion );
    var marker = new google.maps.Marker({
        map:           map,
        position:      ubicacion,
        animation:     google.maps.Animation.DROP,
        markerOptions: {}
    });
    marker.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
            infowindow.setContent("Texto prueba con <b>HTML</b>");
            infowindow.open(map, marker);
        };
    })(marker));
};
// AGREGAR MARKER


// TRAZAR RUTA
var traceRoute = function(origin, destino){
    var direccionDisplay = new google.maps.DirectionsRenderer({
        map: map,
        preserveViewport: true,
        suppressMarkers: true,
        polylineOptions:{
            strokeColor: '#18BC9C',
            strokeOpacity: 0.75,
            strokeWeight: 4
        }
    });

    var request = {
        origin:      origin,
        destination: destino,
        travelMode:  google.maps.TravelMode.DRIVING
    };

    var direccionService = new google.maps.DirectionsService();
    direccionService.route( request, function (response, status) {
        if( status == google.maps.DirectionsStatus.OK ){
            direccionDisplay.setDirections( response );

            updateDistance( response.routes[0].legs[0].distance.value );
        }
    });
};
// TRAZAR RUTA


// UPDATE DISTANCE
var distanceTxt = $('.testMap-container .distance span');
var updateDistance = function( value ){
    distanciaTotal += value;
    distanceTxt.text(distanciaTotal / 1000);
};
// UPDATE DISTANCE
[
	{"lat": 8.294949, "lng": -62.731711},
	{"lat": 8.293898, "lng": -62.731346},
	{"lat": 8.293213, "lng": -62.731107},
	{"lat": 8.292604, "lng": -62.730896},
	{"lat": 8.291779, "lng": -62.730592},
	{"lat": 8.291139, "lng": -62.730343},
	{"lat": 8.290380, "lng": -62.730048},
	{"lat": 8.289660, "lng": -62.729782},
	{"lat": 8.288668, "lng": -62.729316},
	{"lat": 8.287752, "lng": -62.728637},
	{"lat": 8.287115, "lng": -62.729439},
	{"lat": 8.286706, "lng": -62.730018},
	{"lat": 8.286077, "lng": -62.730896},
	{"lat": 8.285355, "lng": -62.730509},
	{"lat": 8.284437, "lng": -62.729978},
	{"lat": 8.282383, "lng": -62.729748},
	{"lat": 8.280997, "lng": -62.728734},
	{"lat": 8.279829, "lng": -62.727795},
	{"lat": 8.278746, "lng": -62.727189},
	{"lat": 8.277467, "lng": -62.728991},
	{"lat": 8.276464, "lng": -62.730633},
	{"lat": 8.274836, "lng": -62.733297},
	{"lat": 8.273490, "lng": -62.735568},
	{"lat": 8.272157, "lng": -62.737974},
	{"lat": 8.272003, "lng": -62.738049}
],

[
	{"lat": 8.293231,	"lng": -62.741472,	"fecha_hora": "2016-04-02 10:30:00"},
	{"lat": 8.295873,	"lng": -62.738062,	"fecha_hora": "2016-04-02 10:30:30"},
	{"lat": 8.297462,	"lng": -62.733425,	"fecha_hora": "2016-04-02 10:31:00"},
	{"lat": 8.298801,	"lng": -62.729599,	"fecha_hora": "2016-04-02 10:31:30"},
	{"lat": 8.301176,	"lng": -62.725124,	"fecha_hora": "2016-04-02 10:32:00"},
	{"lat": 8.304533,	"lng": -62.722093,	"fecha_hora": "2016-04-02 10:32:30"},
	{"lat": 8.307264,	"lng": -62.719531,	"fecha_hora": "2016-04-02 10:33:00"},
	{"lat": 8.304551,	"lng": -62.716085,	"fecha_hora": "2016-04-02 10:33:30"},
	{"lat": 8.300998,	"lng": -62.713180,	"fecha_hora": "2016-04-02 10:34:00"},
	{"lat": 8.296730,	"lng": -62.713901,	"fecha_hora": "2016-04-02 10:34:30"}
],

[
	{ "lat": 8.251666,	"lng": -62.804907, "fecha_hora" : "2016-04-03 17:17:00"},
	{ "lat": 8.251950,	"lng": -62.801670, "fecha_hora" : "2016-04-03 17:17:30"},
	{ "lat": 8.255130,	"lng": -62.798500, "fecha_hora" : "2016-04-03 17:18:00"},
	{ "lat": 8.258042,	"lng": -62.795610, "fecha_hora" : "2016-04-03 17:18:30"},
	{ "lat": 8.261334,	"lng": -62.792413, "fecha_hora" : "2016-04-03 17:19:00"},
	{ "lat": 8.264869,	"lng": -62.788733, "fecha_hora" : "2016-04-03 17:19:30"},
	{ "lat": 8.267577,	"lng": -62.785729, "fecha_hora" : "2016-04-03 17:20:00"},
	{ "lat": 8.271059,	"lng": -62.782360, "fecha_hora" : "2016-04-03 17:20:30"},
	{ "lat": 8.273905,	"lng": -62.778702, "fecha_hora" : "2016-04-03 17:21:00"},
	{ "lat": 8.275147,	"lng": -62.774721, "fecha_hora" : "2016-04-03 17:21:30"},
	{ "lat": 8.270848,	"lng": -62.773155, "fecha_hora" : "2016-04-03 17:22:00"},
	{ "lat": 8.266280,	"lng": -62.771464, "fecha_hora" : "2016-04-03 17:22:30"},
	{ "lat": 8.264125,	"lng": -62.769286, "fecha_hora" : "2016-04-03 17:23:00"},
	{ "lat": 8.265261,	"lng": -62.765006, "fecha_hora" : "2016-04-03 17:23:30"},
	{ "lat": 8.267809,	"lng": -62.763525, "fecha_hora" : "2016-04-03 17:24:00"}
],


[
	{ "lat": 8.281383, "lng":	-62.750196, "fecha_hora": "2016-04-04 22:53:00"},
	{ "lat": 8.277784, "lng":	-62.750679, "fecha_hora": "2016-04-04 22:53:30"},
	{ "lat": 8.274535, "lng":	-62.750475, "fecha_hora": "2016-04-04 22:54:00"},
	{ "lat": 8.271393, "lng":	-62.747310, "fecha_hora": "2016-04-04 22:54:30"},
	{ "lat": 8.269237, "lng":	-62.745132, "fecha_hora": "2016-04-04 22:55:00"},
	{ "lat": 8.270713, "lng":	-62.740315, "fecha_hora": "2016-04-04 22:55:30"},
	{ "lat": 8.271945, "lng":	-62.738019, "fecha_hora": "2016-04-04 22:56:00"}
],

[
	{"lat": 8.271945, "lng": -62.738019, "fecha_hora": "2016-04-05 00:00:00"},
	{"lat": 8.274233, "lng": -62.734083, "fecha_hora": "2016-04-05 00:00:30"},
	{"lat": 8.276802, "lng": -62.729866, "fecha_hora": "2016-04-05 00:01:00"},
	{"lat": 8.277503, "lng": -62.726412, "fecha_hora": "2016-04-05 00:01:30"},
	{"lat": 8.275497, "lng": -62.723150, "fecha_hora": "2016-04-05 00:02:00"},
	{"lat": 8.275465, "lng": -62.719824, "fecha_hora": "2016-04-05 00:02:30"},
	{"lat": 8.278533, "lng": -62.716509, "fecha_hora": "2016-04-05 00:03:00"},
	{"lat": 8.281198, "lng": -62.712196, "fecha_hora": "2016-04-05 00:03:30"},
	{"lat": 8.283566, "lng": -62.708334, "fecha_hora": "2016-04-05 00:04:00"},
	{"lat": 8.283635, "lng": -62.706683, "fecha_hora": "2016-04-05 00:04:30"}
],

[
	{"lat": 8.310548, "lng": -62.671310, "fecha_hora": "2016-04-05 15:55:00"},
	{"lat": 8.306886, "lng": -62.673584, "fecha_hora": "2016-04-05 15:55:30"},
	{"lat": 8.304072, "lng": -62.676492, "fecha_hora": "2016-04-05 15:56:00"},
	{"lat": 8.303191, "lng": -62.681266, "fecha_hora": "2016-04-05 15:56:30"},
	{"lat": 8.299889, "lng": -62.683755, "fecha_hora": "2016-04-05 15:57:00"},
	{"lat": 8.296014, "lng": -62.686373, "fecha_hora": "2016-04-05 15:57:30"},
	{"lat": 8.292543, "lng": -62.688841, "fecha_hora": "2016-04-05 15:58:00"},
	{"lat": 8.288848, "lng": -62.691512, "fecha_hora": "2016-04-05 15:58:30"},
	{"lat": 8.285727, "lng": -62.694505, "fecha_hora": "2016-04-05 15:59:00"},
	{"lat": 8.285599, "lng": -62.699097, "fecha_hora": "2016-04-05 15:59:30"},
	{"lat": 8.285805, "lng": -62.703902, "fecha_hora": "2016-04-05 16:00:00"},
	{"lat": 8.286103, "lng": -62.706767, "fecha_hora": "2016-04-05 16:00:30"},
	{"lat": 8.282981, "lng": -62.709449, "fecha_hora": "2016-04-05 16:01:00"},
	{"lat": 8.280677, "lng": -62.713397, "fecha_hora": "2016-04-05 16:01:30"},
	{"lat": 8.279998, "lng": -62.716626, "fecha_hora": "2016-04-05 16:02:00"}
],

[
	{ "lat": 8.284139,	"lng": -62.713783, "fecha_hora": "2016-04-05 16:45:00"},
	{ "lat": 8.287175,	"lng": -62.716873, "fecha_hora": "2016-04-05 16:45:30"},
	{ "lat": 8.285325,	"lng": -62.719105, "fecha_hora": "2016-04-05 16:46:00"},
	{ "lat": 8.286195,	"lng": -62.722592, "fecha_hora": "2016-04-05 16:46:30"},
	{ "lat": 8.282590,	"lng": -62.723807, "fecha_hora": "2016-04-05 16:47:00"}
]