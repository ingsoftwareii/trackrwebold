var src        = "resources/assets/";
var dest       = "public/";
var components = src + "components";


var gulp   = require('gulp'),
  del      = require('del'),
  stylus   = require('gulp-stylus'),
  quills   = require('quills')
  notify   = require('gulp-notify'),
  plumber  = require('gulp-plumber'),
  jshint   = require('gulp-jshint'),
  stylish  = require('jshint-stylish'),
  bower    = require('main-bower-files'),
  order    = require('gulp-order'),
  concat   = require('gulp-concat'),
  csso     = require('gulp-csso'),
  uglify   = require('gulp-uglify'),
  imagemin = require('gulp-imagemin'),  
  pngquant = require('imagemin-pngquant'),
  path     = require('path'),
  debug    = require('gulp-debug')
  ;



//  CLEAN DEV FOLDERS
gulp.task('clean', del.bind( null, [
  dest + 'css', dest + 'js', dest + 'fonts', dest + 'images'
] ));
gulp.task('clean:components', del.bind( null, [src + 'components'] ));
gulp.task('clean:modules',    del.bind( null, ['node_modules'] ));
gulp.task('clean:all', ['clean', 'clean:components', 'clean:modules'], null);
//  CLEAN DEV FOLDERS



//  COMPILE STYL FILES
gulp.task('stylus', function(){
  return gulp.src(src + 'stylus/*.{stylus,styl}')

  .pipe( plumber({
    errorHandler: function(err){
      notify.onError({
        title:   'Stylus',
        sound:   true,
        message: "Error: <%= error.message %>",
        appIcon: path.join(__dirname, 'gulp-logos/stylus-logo.png')
      })(err);
      // this.emit('end');
    }
  }) )

  .pipe( stylus({ 
    'use':  quills(),
    import: ['quills'] 
  }))

  .pipe(notify({
    title:   'Stylus',
    sound:   true,
    message: "<%= file.relative %> compilado con exito!!",
    appIcon: path.join(__dirname, 'gulp-logos/stylus-logo.png')
  }))

  .pipe( gulp.dest( dest + 'css' ))
});
//  COMPILE STYL FILES



//  LINT JS
gulp.task('lint', function(){
  return gulp.src(src + 'js/**/*.{js,json}')
    .pipe( jshint() )
    .pipe( jshint.reporter(stylish) )
    .pipe( order([
      "**/main.js",
      "**/*.js",
      ]))
    .pipe( concat('main.js'))
    .pipe( notify({
      title:   'JSLint',
      sound:   true,
      message: "<%= file.relative %> compilado!!",
      appIcon: path.join( __dirname, 'gulp-logos/js-logo.png')
    }))
    .pipe( gulp.dest( dest + 'js' ));
});
//  LINT JS



// FONTS
gulp.task('fonts', function() {
  return gulp.src( src + 'fonts/**/*' )
  .pipe( gulp.dest( dest + 'fonts') );
});
// FONTS



//  MINIFY ALL IMAGENES
gulp.task('imagemin', function(){
  return gulp.src( src + 'images/**/*')
    .pipe( imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{
        removeViewBox: false,
        cleanupIDs: false
      }],
      use: [pngquant()]
    }))
    .pipe(gulp.dest(dest + 'images'));
});
//  MINIFY ALL IMAGENES



//  PASS ALL BOWER FILES
gulp.task("bower:css", function(){
  return gulp.src( bower({
    "filter": "**/*.css",
    "overrides": {}
  }) )
  .pipe( debug() )
  .pipe( concat('vendor.min.css'))
  .pipe( csso() )
  .pipe(notify({
    title:   'Vendor Styles',
    sound:   true,
    message: "<%= file.relative %> concatenado y minificado con exito!!",
    appIcon: path.join( __dirname, 'gulp-logos/css-logo.png')
  }))
  .pipe( gulp.dest( dest + 'css'));
});


gulp.task("bower:js", function(){
  return gulp.src( bower({
    "filter": "**/*.js",
    "overrides": {
      "bootstrap": {
        "main": [
          "dist/js/bootstrap.js"
        ]
      }
    }
  }) )
  .pipe( debug() )
  .pipe( concat('vendor.min.js') )
  .pipe( uglify() )
  .pipe(notify({
    title:   'Vendor Scripts',
    sound:   true,
    message: "<%= file.relative %> concatenado y minificado con exito!!",
    appIcon: path.join( __dirname, 'gulp-logos/js-logo.png')
  }))
  .pipe( gulp.dest( dest + 'js'));
});


gulp.task('bower:fonts', function(){
  return gulp.src( components + '/bootstrap/dist/fonts/**/*.*' )
  .pipe(notify({
    title:   'Fonts Scripts',
    sound:   true,
    message: "<%= file.relative %> copiadas con exito!!",
    appIcon: path.join( __dirname, 'gulp-logos/js-logo.png')
  }))
  .pipe( gulp.dest( dest + 'fonts') )
});


gulp.task('bower:all', [ 'bower:css', 'bower:js', 'bower:fonts'], function() {
});
//  PASS JS BOWER FILES



// WATCHERs
gulp.task('watch:stylus', ['stylus'], function(){
  gulp.watch(src + 'stylus/**/*.styl', ['stylus']);
});

gulp.task('watch:js', ['lint'], function(){
  gulp.watch(src + 'js/**/*.js', ['lint']);
});

gulp.task('watch:fonts', ['fonts'], function(){
  gulp.watch( src + 'fonts/**/*' , ['fonts']);
});

gulp.task('watch:images', ['imagemin'], function(){
  gulp.watch(src + 'images/**/*' , ['imagemin']);
});

gulp.task('watch:components', ['bower:all'], function(){
  gulp.watch( components + '/**/*' , ['bower:all'] );
});

gulp.task('watch:all', ['watch:stylus', 'watch:js', 'watch:fonts', 'watch:images', 'watch:components'], function(){
});
// WATCHERs



gulp.task('default', ['watch:all']);

